python scripts as OPCUA server clients for testing
==================================================

- https://github.com/FreeOpcUa/opcua-asyncio
- seems up to date on 24 jan 2020
- only python LGPL license available according to 
https://github.com/open62541/open62541/wiki/List-of-Open-Source-OPC-UA-Implementations

need python >= 3.6

- should be compatible with .ua and also .open6 toolkits

dockerize the opcua-asyncio toolkit:
===================================
- run under cc7 with all python ready installed etc
- make the docker poll for a client script and execute it when injected

cd /home/mludwig/projects/opcua-pyuaf-clients/opcua-asyncio/docker
docker build -f Dockerfile.cc7 .
docker tag XXX gitlab-registry.cern.ch/mludwig/opcua-pyuaf-clients:opcua-asyncio1.0.0
docker push gitlab-registry.cern.ch/mludwig/opcua-pyuaf-clients:opcua-asyncio1.0.0

-----
log into running container:
docker exec -ti XXX /bin/bash
-----

execute an external script:
===========================
1. docker run --net=host --expose=4840-4940 --expose=8800-8900 gitlab-registry.cern.ch/mludwig/opcua-pyuaf-clients:opcua-asyncio1.0.0
        this has to run in the foreground in order to have the trace
2a. (in another terminal) docker ps (to get the container ID)
2b. docker cp ./pilot0.py <containerID>:/client.py
3. the script /client.py is picked up by the loop running in the entry point
