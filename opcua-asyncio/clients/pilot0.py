# clent-minimal.py from
# https://github.com/FreeOpcUa/opcua-asyncio/blob/master/examples/client-minimal.py
#
# OPCUA at 4899
# venus pilot at 4841
import asyncio
import sys
sys.path.insert(0, "..")
import logging
from asyncua import Client, Node, ua

logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger('asyncua')


async def main():
    _logger.info('==entering main===')
    url = 'opc.tcp://localhost:4841'
    # url = 'opc.tcp://commsvr.com:51234/UA/CAS_UA_Server'
    async with Client(url=url) as client:
        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        _logger.info('==getting root node===')
        root = client.get_root_node()
        _logger.info('root node is: %r', root)

        # Node objects have methods to read and write node attributes as well as browse or populate address space
        _logger.info('Children of root are: %r', await root.get_children())

	#INFO:asyncua.client.client:get_namespace_index <class 'list'> ['http://opcfoundation.org/UA/', 			
	#'urn:pcbe13632.cern.ch:JCOP:OpcUaIsegServer', 'OPCUASERVER']
        # uri = 'http://opcua.iseg.examples.freeopcua.github.io'
        uri = 'OPCUASERVER'
        _logger.info('==getting namespace index===')
        idx = await client.get_namespace_index(uri)
        # get a specific node knowing its node id
        # var = client.get_node(ua.NodeId(1002, 2))
        # var = client.get_node("ns=3;i=2002")
        _logger.info('==namespace index= %d', idx)

        _logger.info('===getting var0===')
        node0='ns=2;s=scadaCounters0.health'
        var0=client.get_node(node0)
        _logger.info('===got var0, printing===')
        print("var0= ", var0, await var0.read_value())


        #var = await root.get_child(["0:Objects", f"{idx}:MyObject", f"{idx}:MyVariable"])
        #print("My variable", var, await var.read_value())
        # print(var)
        # var.get_data_value() # get value of node as a DataValue object
        # var.read_value() # get value of node as a python builtin
        # var.write_value(ua.Variant([23], ua.VariantType.Int64)) #set node value using explicit data type
        # var.write_value(3.9) # set node value using implicit data type

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    loop.run_until_complete(main())
    loop.close()

