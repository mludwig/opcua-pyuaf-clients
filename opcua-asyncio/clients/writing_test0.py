# clent-minimal.py from
# https://github.com/FreeOpcUa/opcua-asyncio/blob/master/examples/client-minimal.py
#
# starting a PLC test on a siemens s7 300-400 runnning a test program
# using the opcua test server for this (originally from damian/ben)
# OPCUA at 4841
import asyncio
import sys

sys.path.insert(0, "..")
import logging
from asyncua import Client, Node, ua

#from asyncua.tools import uawrite

from datetime import datetime

logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger('asyncua')



async def main():
    _logger.info('==entering main===')
    url = 'opc.tcp://pcen33951.cern.ch:4841'
    async with Client(url=url) as client:
        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        _logger.info('==getting root node===')
        root = client.get_root_node()
        _logger.info('root node is: %r', root)

        # Node objects have methods to read and write node attributes as well as browse or populate address space
        _logger.info('Children of root are: %r', await root.get_children())

	#INFO:asyncua.client.client:get_namespace_index <class 'list'> ['http://opcfoundation.org/UA/', 			
	#'urn:pcbe13632.cern.ch:JCOP:OpcUaIsegServer', 'OPCUASERVER']
        # uri = 'http://opcua.iseg.examples.freeopcua.github.io'
        # urn:ENICE:OpcUaPLCServer
        # <ServerUri>urn:[NodeName]:CERN:QuasarOpcUaServer</ServerUri> or generic OPCUASERVER
        uri = 'OPCUASERVER'
        _logger.info('==getting namespace index===')
        idx = await client.get_namespace_index(uri)
        # get a specific node knowing its node id
        # var = client.get_node(ua.NodeId(1002, 2))
        # var = client.get_node("ns=3;i=2002")
        _logger.info('==namespace index= %d', idx)

        node='ns=2;s='

        # read variable
        _logger.info('===get===')
        var0name='TEST3_CycleTime_AS.PosSt.value'
        var0NodeName=node+var0name
        var0=client.get_node(var0NodeName)
        var0value=await var0.read_value()
        _logger.info('%s= %r', var0name, var0value)
 
  
        # get/set/get parameter
        _logger.info('===get===')
        par0name='TEST3_BunchesOfIO_APAR.MPosR.value'
        par0NodeName=node+par0name        
        _logger.info('get %s', par0name)
        par0=client.get_node(par0NodeName)
        par0value=await par0.read_value()
        _logger.info('%s= %r', par0name, par0value)
 

        _logger.info('===set===')
        
        ##await par0.write_value(40)

        ##uvar=ua.Variant([23], ua.VariantType.Float)
        ##await par0.write_value(uvar) 
        ##await par0.set_value(44, ua.VariantType.Float)

        # investigate the attributes
        par0attrs=ua.AttributeIds
        await par0.get_attributes( par0attrs )
        _logger.info("par0attrs= %s", par0attrs)
        _logger.info("par0attrs.DataType.value= %s", par0attrs.DataType.value)
        _logger.info("par0attrs.AccessLevel.value= %s", par0attrs.AccessLevel.value)
        _logger.info("par0attrs.UserAccessLevel.value= %s", par0attrs.UserAccessLevel.value)

        # get the datavalue
        _logger.info("==> get dv from %s", par0name)
        dv0=await par0.get_data_value()
        _logger.info("dv0= %s", dv0)
        _logger.info("dv0.SourceTimestamp= %s", dv0.SourceTimestamp)
        _logger.info("dv0.ServerTimestamp= %s", dv0.ServerTimestamp)
        _logger.info("dv0.Value= %s", dv0.Value)
        _logger.info("dv0.Value.VariantType= %s", dv0.Value.VariantType)
        _logger.info("dv0.StatusCode= %s", dv0.StatusCode)
        _logger.info("dv0.Value.Value= %s", dv0.Value.Value)

        open6=True
        #open6=False
        if open6:
           #----this works for .open6---
           _logger.info("WRITE FOR OPEN6")
           dv0.Value.Value=15
           # don't touch timestamps
           #  dv0.SourceTimestamp=None
           #  dv0.ServerTimestamp=None
        else:
           #---- for .ua this does not yet work, maybe credentials are also needed (session)---
           # https://github.com/node-opcua/node-opcua/issues/462
           _logger.info("WRITE FOR UA")
           dv0.Value.Value=13
           dv0.SourceTimestamp=datetime.now()
           dv0.ServerTimestamp=datetime.now()
           #dv0.SourceTimestamp=""
           #dv0.ServerTimestamp=None
           dv0.StatusCode=StatusCode.Good

        _logger.info("sourceTS=%s serverTS=%s", dv0.SourceTimestamp, dv0.ServerTimestamp)
        await par0.set_data_value(dv0) 
 

        _logger.info("==> set dv1 to %s", par0name)

        #dv1 = ua.DataValue()        
        dv1 = ua.DataValue([44], ua.VariantType.Float )        
        #dv1.ServerTimestamp = datetime.now()
        #dv1.SourceTimestamp = datetime.now()
        #dv1.ServerTimestamp = None
        #dv1.SourceTimestamp = None
        #dv1.Value=ua.Variant([44.4], ua.VariantType.Float)
        #await par0.set_writable( True )
       # await par0.set_data_value( dv1 )

        # read back
        _logger.info('===get===')
        _logger.info(par0name)
        par0value=await par0.read_value()
        _logger.info('%s= %r', par0name, par0value)


        #var = await root.get_child(["0:Objects", f"{idx}:MyObject", f"{idx}:MyVariable"])
        #print("My variable", var, await var.read_value())
        # print(var)
        # var.get_data_value() # get value of node as a DataValue object
        # var.read_value() # get value of node as a python builtin
        # var.write_value(ua.Variant([23], ua.VariantType.Int64)) #set node value using explicit data type
        # var.write_value(3.9) # set node value using implicit data type

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    loop.run_until_complete(main())
    loop.close()




