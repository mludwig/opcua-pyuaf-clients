#!/bin/bash

#/Python-3.6.2/python --version
#curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
#/Python-3.6.2/python get-pip.py
## from https://github.com/FreeOpcUa/opcua-asyncio
#pip install asyncua

# we should pull these clients from a repo somewhere, not delivering them with the docker
#echo "running asyncua python client PYPROGRAM= /clients/${PYPROGRAM}"
#/Python-3.6.2/python /clients/${PYPROGRAM}

#echo "keeping docker alive "
#while true
#do
#    sleep 10
#done
#echo "===containder id===
#cat /proc/self/cgroup | grep -o  -e "docker-.*.scope" | head -n 1 | sed "s/docker-\(.*\).scope/\\1/

echo "looking for script to execute "
echo "inject a script like: docker cp mytest0.py <mycontainerID>:/client.py"
while true
	do
	if [ -f /client.py ]
	then
	    echo "/client.py file exists, execute it once"
	    /Python-3.6.2/python /client.py
	    echo "/client.py done, removing from container."
            rm /client.py
            echo "==="
            echo "inject a script like: docker cp mytest0.py mycontainer:/client.py"
            echo "==="
	fi
        sleep 1
done


