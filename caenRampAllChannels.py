'''
(c) Copyright CERN 2019. All  rights  reserved.  This  software  is  released  under  a 
CERN proprietary  software  licence.  Any  permission to  use  it  shall  be  granted
in  writing.  Requests shall be addressed to CERN through mail 
- author(s)
- KT@cern.ch

author(s): 
    Michael Ludwig BE-ICS-FD michael.ludwig@cern.ch
'''
# CAEN OPCUA tests (venus)
# simple read/write and increment test on scalars, and arrays
# Python 2.7.5 on cc7 3.10.0-957.21.2.el7.x86_64
#
# PYTHONPATH=/home/mludwig/projects/OPCUA-pyuaf-clients:/home/mludwig/3rdPartySoftware/UAF/uaf/lib

#import numpy as np
import ctypes
import time
import sys

import pyuaf
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
from pyuaf.util.primitives  import Byte, UInt32, Int32, Float, Double, String

# internal macros
NOT_FOUND = "not found"

# ---------------------

# read a number from a given channel, crate is implicit global, myclient is global as well
# use JCOP naming conventions: Board00 Chan000
# crate and board are integers
def readChProp(board, channel, prop): 
    sboard = ".Board"+str( board ).zfill(2)
    schannel = ".Chan"+str( channel ).zfill(3)
    addr = Address(NodeId(globalCrate0+sboard+schannel+"."+prop, globalName), globalUrn )  
    #print("reading %s" %addr.getExpandedNodeId())
    result = myClient.read( [addr] )
    #print("addr overallStatus= %s" %result.overallStatus )
    #print("addr targets.data= %s" %result.targets[0].data )
    if isinstance(result.targets[0].data, Float):   # <pyuaf.util.primitives.Float(0)>
        return Float(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, Int32): # <pyuaf.util.primitives.Int32(0)>
        return Int32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, UInt32): # <pyuaf.util.primitives.Int32(0)>
        return UInt32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, String): 
        return str(result.targets[0].data)
    else: 
        return NOT_FOUND


# read a property from a given board/slot, crate is implicit global, myclient is global as well
# use JCOP naming conventions: Board00 
# crate and board are integers
# returns: 
#    float if OK
#    "not found" if it does not exists
def readBdProp(board, prop): 
    sboard = ".Board"+str( board ).zfill(2)
    addr = Address(NodeId(globalCrate0+sboard+"."+prop, globalName), globalUrn )  
     #print("reading %s" %addr.getExpandedNodeId())
    result = myClient.read( [addr] )
    #print("result.targets[0].data= ", result.targets[0].data )    
    if isinstance(result.targets[0].data, Float):   # <pyuaf.util.primitives.Float(0)>
        return Float(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, Int32): # <pyuaf.util.primitives.Int32(0)>
        return Int32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, UInt32): # <pyuaf.util.primitives.Int32(0)>
        return UInt32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, String):  # <pyuaf.util.primitives.String(A1676)>
        return str(result.targets[0].data)
    else: 
        return NOT_FOUND

# read a numerical property from a crate/controller. Crate is implicitly global,
# myclient is global as well
def readCrateProp(prop): 
    addr = Address(NodeId(globalCrate0+"."+prop, globalName), globalUrn )  
    print("reading %s" %addr.getExpandedNodeId())
    result = myClient.read( [addr] )
    print("addr overallStatus= %s" %result.overallStatus )
    print("addr targets.data= %s" %result.targets[0].data )
    print("result.targets[0].data= ", result.targets[0].data )
    if isinstance(result.targets[0].data, Float):   # <pyuaf.util.primitives.Float(0)>
        return Float(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, Int32): # <pyuaf.util.primitives.Int32(0)>
        return Int32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, UInt32): # <pyuaf.util.primitives.Int32(0)>
        return UInt32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, String): # <pyuaf.util.primitives.String(A1676)>
        return str(result.targets[0].data)
    else: 
        return NOT_FOUND

# print out namespace, uri etc of the server discovery from pyuaf
def checkServer(): 
    addr = Address(NodeId(globalCrate0+".Slots", globalName), globalUrn )  
    print("reading %s" %addr.getExpandedNodeId())
    result = myClient.read( [addr] )
    print("addr overallStatus= %s" %result.overallStatus )
    print("addr targets.data= %s" %result.targets[0].data )
    print("result.targets[0].data= ", result.targets[0].data )
    print("result= ", result )
    print("===made it up to here, caen seems good===")

# set a number for a given channel, crate is implicit global, myclient is global as well
# use JCOP naming conventions: Board00 Chan000
# crate and board are integers
# we are actually writing floats, and rely on internal conversion of types
def writeChPropNumerical(board, channel, prop, newvalue): 
    sboard = ".Board"+str( board ).zfill(2)
    schannel = ".Chan"+str( channel ).zfill(3)   
    addr = Address(NodeId(globalCrate0+sboard+schannel+"."+prop, globalName), globalUrn )  
    result = myClient.write( [addr], [ Float(newvalue) ] )

# tries to read Model and returns a list of populated primary slots in the crate (=boards)
# obviously only populated slots can return a board "Model", so the other ones
# are empty
def tryBoards( maxSlots ):
    populatedSlots = []
    emptySlots = []
    for slot in range (maxSlots):
        result = readBdProp(slot, "Model")
        #print( "result= ", result, isinstance(result, str))
        if isinstance(result, str):
            if ( result == NOT_FOUND ):
                #print("slot ", slot, " is empty")
                emptySlots.append(slot)
            else:
                #print("slot ", slot, " has a board ", result)
                populatedSlots.append(slot)
        else:
            populatedSlots.append(slot)
    return populatedSlots
    
# find out if a given board, channel is an EASY marker channel
# we need this in order to skip these marker channels for
# normal power supply operations.
# we check if channel property "SVMax" exists:
# if yes = channel is power channel
# if no = channel is EASY marker channel
def isCaenEasyMarkerChannel( sl, ch ):
    result = readChProp(sl, ch, "SVMax")
    if isinstance(result, str):
        if( result == NOT_FOUND ):
            #print("EASY MARKER channel ", sl, ch)
            return True
        else:
            #print("power channel ", sl, ch)
            return False
  

# see if all power supply channels of all boards show VMon==0
# skip EASY marker channels
def waitForVMonZero( boards ): 
    allChannelsPoweredDown = False
    nbBoards = len(boards)
    while allChannelsPoweredDown != True:
        print("checking VMon==0 on all power channels")
        allChannelsPoweredDown = True
        for bd in range (nbBoards):
            sl = boards[bd]
            #print("checking VMon on all power channels of board= ", sl)
            nbChannels = readBdProp(sl, "NrOfCh").value 
            for ch in range (nbChannels):
                if isCaenEasyMarkerChannel( sl, ch):
                    #print(sl, ch, "is an EASY marker channel, skipping")
                    continue
                vmon = readChProp(sl, ch, "VMon").value
                if vmon > 0.1:
                    allChannelsPoweredDown = False
                    print("board in slot ", sl, " channel ", ch, " still with tension: VMon= ", vmon)
  
        time.sleep(1)

# set any property to a num value for all power supply channels of all boards
# we skip EASY marker channels
def writeNumericalToBoardsPSchannels( boards, prop, value ):
    nbBoards = len(boards)
    for bd in range (nbBoards):
        sl = boards[bd]
        nbChannels = readBdProp(sl, "NrOfCh").value
        for ch in range (nbChannels):
            if isCaenEasyMarkerChannel( sl, ch):
                #print(sl, ch, "is an EASY marker channel, skipping")
                continue
            writeChPropNumerical(sl, ch, prop, value)

# writes any numerical property value to all power supply channels of a given board
# the board is given as populated slot, take care yourself for it to exist
def writeNumericalToBoardPSchannels( slot, prop, value ):
        nbChannels = readBdProp(slot, "NrOfCh").value
        for ch in range (nbChannels):
            if isCaenEasyMarkerChannel( slot, ch):
                #print(slot, ch, "is an EASY marker channel, skipping")
                continue
            writeChPropNumerical(slot, ch, prop, value)


# pure software ramp up all boards and channels for power supplies, using V0Set and Pw
# takes either to maximum possible voltage of each board (SVMax) or a max limit set (vmax)
# like this we 
#    * respect max voltages for low volt boards
#    * ramp HV chanels just to vmax to save some time
# whichever is lower.
def startRampUp( boards, vmax, rampSpeedUp, rampSpeedDown ):
    nbBoards = len(boards)
    for bd in range (nbBoards):
        sl = boards[bd]
        nbChannels = readBdProp(sl, "NrOfCh").value
        # print("board in slot= ", sl, " has ", nbChannels, " channels (including EASY markers)")
        for ch in range (nbChannels):
            if isCaenEasyMarkerChannel( sl, ch):
                continue
        
            svmax = readChProp(sl, ch, "SVMax").value
            writeChPropNumerical(sl, ch, "RUp", rampSpeedUp)
            writeChPropNumerical(sl, ch, "RDWn", rampSpeedDown)
                
            # avoid endless ramping for very high voltage boards, but respect limits for
            # for low volt boards to avoid trips
            if svmax > vmax:
                writeChPropNumerical(sl, ch, "V0Set", vmax)
            else:
                writeChPropNumerical(sl, ch, "V0Set", svmax)
            writeChPropNumerical(sl, ch, "Pw", 1)


# wait for ramp to finish, checking all boards in sequence 
# compare V0Set to VMon, but allow for a 10% margin on target (there is noise)  
def waitForV0SetTarget( boards ):
    nbBoards = len(boards)
    nbBoardsOnTarget = 0
    while nbBoardsOnTarget < nbBoards:
        print("\n===checking if all boards are on target==")    
        nbBoardsOnTarget = 0
        for bd in range (nbBoards):
            sl = boards[bd]
            nbChannels = readBdProp(sl, "NrOfCh").value
            nbChannelsOnTarget = 0
            #print("checking if board= ", sl, " with ", nbChannels, " channels is on target")    
            for ch in range (nbChannels):
                if isCaenEasyMarkerChannel( sl, ch):
                    nbChannelsOnTarget += 1
                    continue
                        
                newVMon = readChProp(sl, ch, "VMon").value
                targetVMon = readChProp(sl, ch, "V0Set").value * 0.9
                # print(sl, ch, "VMon= ", newVMon, " targetVMon= ", targetVMon )     

                if newVMon > targetVMon: 
                    nbChannelsOnTarget += 1
                   
            if ( nbChannelsOnTarget == nbChannels ):
                nbBoardsOnTarget += 1
                print("board in slot= ", sl, " is on target, have ", nbBoardsOnTarget, " boards on target, need ", nbBoards)    
            else:
                print("board= ", sl, " with ", nbChannels, " channels not yet on target")
            time.sleep(1)
           
def isBlank (myString):
    if myString and myString.strip():
        #myString is not None AND myString is not empty or blank
        return False
    #myString is None OR myString is empty or blank
    return True# ---------------------



# create a client(connection) named "myClient". We need:
# -port: configured in ServerConfig.xml
# -ip or hostname: computer
# - name of the crate=globalCrate0: as configured to the adress space (config.xml)
# -uri=globalUrn: as configured in the ServerConfig.xml
# -global name=namespace in the adress space: configured in ServerConfix.xml
serverHostNameDefault = "localhost"
opcUaPortDefault = "4901"
globalCrate0 = "simSY4527"
globalUrn = "urn:JCOP:OpcUaCaenServer"
globalName = "opcuaserver" 
# the urn and uri are configured in the according ServerConfix.xml, at startup of the OPCUACaenServer
'''
  <ApplicationUri>urn:JCOP:OpcUaCaenServer</ApplicationUri>
  <ServerUri>urn:[NodeName]:JCOP:OpcUaCaenServer</ServerUri>
'''

    
myClient = Client(ClientSettings("pyuaf_ramp_all_channels", ["opc.tcp://venus-combo1:4841"]))
checkServer()

print( "hit any key to start")
sys.stdin.readline()


# find out, by trying, how many primary boards are actually present, and return a 
# vector with populated slots = boards
nbSlots = readCrateProp("Slots")
print("nbSlots= ", nbSlots.value )

boards = tryBoards( int(nbSlots.value) )
print("populated slots/boards found= ", boards)
print("total populated slots/boards found= ", len(boards) )

print("\n===switch off Pw=0, V0Set=0 all channels===")
writeNumericalToBoardsPSchannels( boards, "V0Set", 0 )
writeNumericalToBoardsPSchannels( boards, "Pw", 0 )
print("\n===all channels are powering down, wait===")
waitForVMonZero( boards ) 
print("\n===all channels are really at VMon=0 now===")
time.sleep(5)


start = time.time()

print("\n===starting RAMP UP up all boards and channels===")
newV0Set = 300
rampSpeedUp = 20
rampSpeedDown = 20
print("parameters: ", newV0Set, rampSpeedUp, rampSpeedDown)
startRampUp( boards, newV0Set, rampSpeedUp, rampSpeedDown )

print("\n===RAMP UP up all boards and channels started, waiting===")
waitForV0SetTarget( boards )

end = time.time()
print("ramp up took ", end - start)

print("\n===RAMP UP up all boards and channels done, keep ON for 10 secs===")
time.sleep(10)
print("\n===starting RAMP DOWN up all boards and channels===")

# ramp all boards and channel down by switching them off, actually real 0.0
# and wait for ramp to finish        
writeNumericalToBoardsPSchannels( boards, "Pw", 0 )
print("\n===RAMP DOWN up all boards and channels started, waiting===")
waitForVMonZero( boards ) 
print("\n===all channels are really at VMon=0 now===")
print("(repeat) ramp up took ", end - start)


