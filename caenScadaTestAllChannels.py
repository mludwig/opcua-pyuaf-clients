'''
(c) Copyright CERN 2019. All  rights  reserved.  This  software  is  released  under  a 
CERN proprietary  software  licence.  Any  permission to  use  it  shall  be  granted
in  writing.  Requests shall be addressed to CERN through mail 
- author(s)
- KT@cern.ch

author(s): 
    Michael Ludwig BE-ICS-FD michael.ludwig@cern.ch
'''
# CAEN OPCUA tests (venus)
# simple read/write and increment test on scalars, and arrays
# Python 2.7.5 on cc7 3.10.0-957.21.2.el7.x86_64
#
# PYTHONPATH=/home/mludwig/projects/OPCUA-pyuaf-clients:/home/mludwig/3rdPartySoftware/UAF/uaf/lib

#import numpy as np
import ctypes
import time
import sys

import pyuaf
# from pyuaf.util             import Address, NodeId
from pyuaf.util             import *
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
from pyuaf.util.primitives  import Byte, UInt32, Int32, Float, Double, String
from pyuaf.util.errors      import UnknownServerError, UafError
from pyuaf.util.opcuatypes import Boolean

# internal macros
NOT_FOUND = "not found"

# ---VENUS PILOT----------
def readPilotProp( node, prop): 
    addr = Address(NodeId( node+"."+prop, pilotName), pilotUrn )  
    print("pilot reading %s" %addr.getExpandedNodeId())
    result = pilot.read( [addr] )
    print("pilot read overallStatus= %s" %result.overallStatus )
    print("pilot read targets.data= %s" %result.targets[0].data )
    if isinstance(result.targets[0].data, Float):   # <pyuaf.util.primitives.Float(0)>
        return Float(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, Int32): # <pyuaf.util.primitives.Int32(0)>
        return Int32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, UInt32): # <pyuaf.util.primitives.Int32(0)>
        return UInt32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, String): 
        return str(result.targets[0].data)
    else: 
        return NOT_FOUND

def writePilotPropInteger(node, prop, newvalue): 
    addr = Address(NodeId( node+"."+prop, pilotName), pilotUrn )  
    result = pilot.write( [addr], [ Int32(newvalue) ] )
    print("pilot write overallStatus= %s" %result.overallStatus )

def writePilotPropBool(node, prop, newvalue): 
    addr = Address(NodeId( node+"."+prop, pilotName), pilotUrn )  
    result = pilot.write( [addr], [ bool(newvalue) ] )
    print("pilot write overallStatus= %s" %result.overallStatus )

def writePilotPropFloat2dim(node, prop, newvalue): 
    addr = Address(NodeId( node+"."+prop, pilotName), pilotUrn )  
    result = pilot.write( [addr], [ newvalue ] )
    print("pilot write overallStatus= %s" %result.overallStatus )


# print out namespace, uri etc of the server discovery from pyuaf
def checkPilot(): 
    addr = Address(NodeId( "genericPilot0.config", pilotName), pilotUrn )  
    print("reading %s" %addr.getExpandedNodeId())
    result = pilot.read( [addr] )
    print("addr overallStatus= %s" %result.overallStatus )
    print("addr targets.data= %s" %result.targets[0].data )
    print("result.targets[0].data= ", result.targets[0].data )
    print("result= ", result )
    print("===made it up to here, pilot seems good===")



# -----CAEN----------------

# read a number from a given channel, crate is implicit global, myclient is global as well
# use JCOP naming conventions: Board00 Chan000
# crate and board are integers
def readChProp(board, channel, prop): 
    sboard = ".Board"+str( board ).zfill(2)
    schannel = ".Chan"+str( channel ).zfill(3)
    addr = Address(NodeId(globalCrate0+sboard+schannel+"."+prop, globalName), globalUrn )  
    #print("reading %s" %addr.getExpandedNodeId())
    result = myClient.read( [addr] )
    #print("addr overallStatus= %s" %result.overallStatus )
    #print("addr targets.data= %s" %result.targets[0].data )
    if isinstance(result.targets[0].data, Float):   # <pyuaf.util.primitives.Float(0)>
        return Float(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, Int32): # <pyuaf.util.primitives.Int32(0)>
        return Int32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, UInt32): # <pyuaf.util.primitives.Int32(0)>
        return UInt32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, String): 
        return str(result.targets[0].data)
    else: 
        return NOT_FOUND


# read a property from a given board/slot, crate is implicit global, myclient is global as well
# use JCOP naming conventions: Board00 
# crate and board are integers
# returns: 
#    float if OK
#    "not found" if it does not exists
def readBdProp(board, prop): 
    sboard = ".Board"+str( board ).zfill(2)
    addr = Address(NodeId(globalCrate0+sboard+"."+prop, globalName), globalUrn )  
     #print("reading %s" %addr.getExpandedNodeId())
    result = myClient.read( [addr] )
    #print("result.targets[0].data= ", result.targets[0].data )    
    if isinstance(result.targets[0].data, Float):   # <pyuaf.util.primitives.Float(0)>
        return Float(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, Int32): # <pyuaf.util.primitives.Int32(0)>
        return Int32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, UInt32): # <pyuaf.util.primitives.Int32(0)>
        return UInt32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, String):  # <pyuaf.util.primitives.String(A1676)>
        return str(result.targets[0].data)
    else: 
        return NOT_FOUND

# read a numerical property from a crate/controller. Crate is implicitly global,
# myclient is global as well
def readCrateProp(prop): 
    addr = Address(NodeId(globalCrate0+"."+prop, globalName), globalUrn )  
    print("reading %s" %addr.getExpandedNodeId())
    result = myClient.read( [addr] )
    print("addr overallStatus= %s" %result.overallStatus )
    print("addr targets.data= %s" %result.targets[0].data )
    print("result.targets[0].data= ", result.targets[0].data )
    if isinstance(result.targets[0].data, Float):   # <pyuaf.util.primitives.Float(0)>
        return Float(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, Int32): # <pyuaf.util.primitives.Int32(0)>
        return Int32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, UInt32): # <pyuaf.util.primitives.Int32(0)>
        return UInt32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, String): # <pyuaf.util.primitives.String(A1676)>
        return str(result.targets[0].data)
    else: 
        return NOT_FOUND

# print out namespace, uri etc of the server discovery from pyuaf
def checkServer(): 
    addr = Address(NodeId(globalCrate0+".Slots", globalName), globalUrn )  
    print("reading %s" %addr.getExpandedNodeId())
    result = myClient.read( [addr] )
    print("addr overallStatus= %s" %result.overallStatus )
    print("addr targets.data= %s" %result.targets[0].data )
    print("result.targets[0].data= ", result.targets[0].data )
    print("result= ", result )
    print("===made it up to here, caen seems good===")

# set a number for a given channel, crate is implicit global, myclient is global as well
# use JCOP naming conventions: Board00 Chan000
# crate and board are integers
# we are actually writing floats, and rely on internal conversion of types
def writeChPropNumerical(board, channel, prop, newvalue): 
    sboard = ".Board"+str( board ).zfill(2)
    schannel = ".Chan"+str( channel ).zfill(3)   
    addr = Address(NodeId(globalCrate0+sboard+schannel+"."+prop, globalName), globalUrn )  
    result = myClient.write( [addr], [ Float(newvalue) ] )

# tries to read Model and returns a list of populated primary slots in the crate (=boards)
# obviously only populated slots can return a board "Model", so the other ones
# are empty
def tryBoards( maxSlots ):
    populatedSlots = []
    emptySlots = []
    for slot in range (maxSlots):
        result = readBdProp(slot, "Model")
        #print( "result= ", result, isinstance(result, str))
        if isinstance(result, str):
            if ( result == NOT_FOUND ):
                #print("slot ", slot, " is empty")
                emptySlots.append(slot)
            else:
                #print("slot ", slot, " has a board ", result)
                populatedSlots.append(slot)
        else:
            populatedSlots.append(slot)
    return populatedSlots
    
# find out if a given board, channel is an EASY marker channel
# we need this in order to skip these marker channels for
# normal power supply operations.
# we check if channel property "SVMax" exists:
# if yes = channel is power channel
# if no = channel is EASY marker channel
def isCaenEasyMarkerChannel( sl, ch ):
    result = readChProp(sl, ch, "SVMax")
    if isinstance(result, str):
        if( result == NOT_FOUND ):
            #print("EASY MARKER channel ", sl, ch)
            return True
        else:
            #print("power channel ", sl, ch)
            return False
  

# see if all power supply channels of all boards show VMon==0
# skip EASY marker channels
def waitForVMonZero( boards ): 
    allChannelsPoweredDown = False
    nbBoards = len(boards)
    while allChannelsPoweredDown != True:
        # print("checking VMon==0 on all power channels")
        allChannelsPoweredDown = True
        for bd in range (nbBoards):
            sl = boards[bd]
            #print("checking VMon on all power channels of board= ", sl)
            nbChannels = readBdProp(sl, "NrOfCh").value 
            for ch in range (nbChannels):
                if isCaenEasyMarkerChannel( sl, ch):
                    #print(sl, ch, "is an EASY marker channel, skipping")
                    continue
                vmon = readChProp(sl, ch, "VMon").value
                if vmon > 0.1:
                    allChannelsPoweredDown = False
                    print("board in slot ", sl, " channel ", ch, " still with tension: VMon= ", vmon)
  
        time.sleep(1)

# set any property to a num value for all power supply channels of all boards
# we skip EASY marker channels
def writeNumericalToBoardsPSchannels( boards, prop, value ):
    nbBoards = len(boards)
    for bd in range (nbBoards):
        sl = boards[bd]
        nbChannels = readBdProp(sl, "NrOfCh").value
        for ch in range (nbChannels):
            if isCaenEasyMarkerChannel( sl, ch):
                #print(sl, ch, "is an EASY marker channel, skipping")
                continue
            writeChPropNumerical(sl, ch, prop, value)

# writes any numerical property value to all power supply channels of a given board
# the board is given as populated slot, take care yourself for it to exist
def writeNumericalToBoardPSchannels( slot, prop, value ):
        nbChannels = readBdProp(slot, "NrOfCh").value
        for ch in range (nbChannels):
            if isCaenEasyMarkerChannel( slot, ch):
                #print(slot, ch, "is an EASY marker channel, skipping")
                continue
            writeChPropNumerical(slot, ch, prop, value)


# pure software ramp up all boards and channels for power supplies, using V0Set and Pw
# takes either to maximum possible voltage of each board (SVMax) or a max limit set (vmax)
# like this we 
#    * respect max voltages for low volt boards
#    * ramp HV chanels just to vmax to save some time
# whichever is lower.
def startRampUpByPw( boards, vmax, rampSpeedUp, rampSpeedDown ):
    nbBoards = len(boards)
    for bd in range (nbBoards):
        sl = boards[bd]
        nbChannels = readBdProp(sl, "NrOfCh").value
        # print("board in slot= ", sl, " has ", nbChannels, " channels (including EASY markers)")
        for ch in range (nbChannels):
            if isCaenEasyMarkerChannel( sl, ch):
                continue
            writeChPropNumerical(sl, ch, "Pw", 1)

# set all channels up for a "physicist ramp" using V0Set and V1Set
# and I0Set and I1Set, together with ISEL and VSEL
# like this we can run into trips 
# V0Set is 10% of SVMax
def setRampAllChannels( boards, v0max, v1max, i0, i1, rampSpeedUp, rampSpeedDown ):
    nbBoards = len(boards)
    for bd in range (nbBoards):
        sl = boards[bd]
        nbChannels = readBdProp(sl, "NrOfCh").value
        # print("board in slot= ", sl, " has ", nbChannels, " channels (including EASY markers)")
        for ch in range (nbChannels):
            if isCaenEasyMarkerChannel( sl, ch):
                continue
        
            writeChPropNumerical(sl, ch, "RUp", rampSpeedUp)
            writeChPropNumerical(sl, ch, "RDWn", rampSpeedDown)
                
            # avoid endless ramping for very high voltage boards, but respect limits for
            # for low volt boards. We can actually have current trips
            svmax = readChProp(sl, ch, "SVMax").value
            if svmax * 0.1 > v0max:
                writeChPropNumerical(sl, ch, "V0Set", v0max )
            else:
                writeChPropNumerical(sl, ch, "V0Set", svmax * 0.1 )

            if svmax > v1max:
                writeChPropNumerical(sl, ch, "V1Set", v1max )
            else:
                writeChPropNumerical(sl, ch, "V1Set", svmax )
            writeChPropNumerical(sl, ch, "I0Set", i0 )
            writeChPropNumerical(sl, ch, "I1Set", i1 )
                
                
                
                


# wait for ramp to finish, checking all boards in sequence 
# compare V0Set or V1Set (=Vtarget as a string) to VMon, 
# but allow for a 10% margin on target (there is noise)  
def waitForSetTarget( boards, Vtarget ):
    nbBoards = len(boards)
    nbBoardsOnTarget = 0
    while nbBoardsOnTarget < nbBoards:
        # print("\n===checking if all boards are on target==")    
        nbBoardsOnTarget = 0
        for bd in range (nbBoards):
            sl = boards[bd]
            nbChannels = readBdProp(sl, "NrOfCh").value
            nbChannelsOnTarget = 0
            #print("checking if board= ", sl, " with ", nbChannels, " channels is on target")    
            for ch in range (nbChannels):
                if isCaenEasyMarkerChannel( sl, ch):
                    nbChannelsOnTarget += 1
                    continue
                        
                newVMon = readChProp(sl, ch, "VMon").value
                targetVMon = readChProp(sl, ch, Vtarget).value * 0.9
                # print(sl, ch, "VMon= ", newVMon, " targetVMon= ", targetVMon )     

                if newVMon > targetVMon: 
                    nbChannelsOnTarget += 1
                   
            if ( nbChannelsOnTarget == nbChannels ):
                nbBoardsOnTarget += 1
                print("board in slot= ", sl, " is on target, have ", nbBoardsOnTarget, " boards on target, need ", nbBoards)    
            else:
                print("board= ", sl, " with ", nbChannels, " channels not yet on target")
            time.sleep(2)
           
           
           
           
           
def isBlank (myString):
    if myString and myString.strip():
        #myString is not None AND myString is not empty or blank
        return False
    #myString is None OR myString is empty or blank
    return True# ---------------------



# create a client(connection) named "myClient". We need:
# -port: configured in ServerConfig.xml
# -ip or hostname: computer
# - name of the crate=globalCrate0: as configured to the adress space (config.xml)
# -uri=globalUrn: as configured in the ServerConfig.xml
# -global name=namespace in the adress space: configured in ServerConfix.xml

#serverHostNameDefault = "localhost"
#opcUaPortDefault = "4841"  # the debugging version, normally we are on 4901
globalCrate0 = "simSY4527"
globalUrn = "urn:JCOP:OpcUaCaenServer"
globalName = "opcuaserver" 

pilotUrn = "urn:CERN:QuasarOpcUaServer"
# open6 would be available, but the pyuaf isn't happy
# https://uaf.github.io/uaf/doc/pyuaf/api_pyuaf_util_securitypolicies.html#pyuaf.util.securitypolicies.UA_Basic128
# pilotUrn = "http://open62541.org"
# and we get a certificate error:
# pyuaf.util.errors.ServerDidNotProvideCertificateError: The server did not provide a certificate
#
# have to convince pyuaf to have session security setting NONE since open6 does not provide
# a certificate
#
#pilotClientSettings = pilot.clientSettings()
#pilotDefault = pilotClientSettings.defaultSessionSettings()

pilotName = "opcuaserver" 


# the urn and uri are configured in the according ServerConfix.xml, at startup of the OPCUACaenServer
'''
  <ApplicationUri>urn:JCOP:OpcUaCaenServer</ApplicationUri>
  <ServerUri>urn:[NodeName]:JCOP:OpcUaCaenServer</ServerUri>
'''

    
#myClient = Client(ClientSettings("pyuaf_scada_test_all_channels", ["opc.tcp://venus-combo0:4841"]))
myClient = Client(ClientSettings("pyuaf_scada_test_all_channels", ["opc.tcp://localhost:4841"]))
print("caen found server: ")
print(myClient.serversFound())
checkServer()

#pilot = Client(ClientSettings("pyuaf_scada_test_all_channels", ["opc.tcp://venus-combo0:8890"]))
pilot = Client(ClientSettings("pyuaf_scada_test_all_channels", ["opc.tcp://localhost:8890"]))
print("pilot found server: ")
print(pilot.serversFound())
checkPilot()

readPilotProp( "genericPilot0", "config") 
readPilotProp( "scadaCounters0", "health") 

'''
just an example how to do it ;-)

readPilotProp( "genericPilot0", "I_channel") 
writePilotPropInteger( "genericPilot0", "I_channel", 1 ) 
readPilotProp( "genericPilot0", "I_channel") 


readPilotProp( "genericPilot0", "channelLoad")  # I_slot, I_channel
ar = [Float(100.1), Float(101.1)]
writePilotPropFloat2dim("genericPilot0", "channelLoad", ar ) 
readPilotProp( "genericPilot0", "channelLoad")  # I_slot, I_channel 
'''

# do some basic caen OPC server discovery on the AS
nbSlots = readCrateProp("Slots")
print("caen: nbSlots= ", nbSlots.value )

boards = tryBoards( int(nbSlots.value) )
print("caen: populated slots/boards found= ", boards)
print("caen: total populated slots/boards found= ", len(boards) )

#for big setups  we get timeout problems with pyuaf because servers are slow
# would need to increase client timeouts 

print( "\n\n===hit any key to start===\n\n")
sys.stdin.readline()
start = time.time()

# switch off scada counter increment and sending for now
print("===pilot: switch off counters===")
readPilotProp( "scadaCounters0", "scadaCounterFlag")   
writePilotPropBool( "scadaCounters0", "scadaCounterFlag", 0 ) 
readPilotProp( "scadaCounters0", "scadaCounterFlag")   

# set the loads of all channels, 1kOhm, 1uF, so we read back in mA
print("===pilot: setting all loads===")
allLoads = [Float(1000.0), Float(10e-6)]
writePilotPropFloat2dim("genericPilot0", "allChannelsLoad", allLoads ) 

# power off all channels
print("===pilot: power off all channels===")
writePilotPropBool("genericPilot0", "powerAllChannels", 0 ) 

# check with the caen OPC server that all channels show VMon==0
print("===caen: check and wait until VMon==0 all channels===")
waitForVMonZero( boards ) 
print("===caen: all channels are really at VMon=0 now===")

# set ramp V0Set, V1Set, I0Set and I1Set plus the speeds but don't start it yet
# respect also SVMax limits
# since we have set 1kOhm, we'll have 30mA and 300mA at the plateaus
# go slowly to enjoy the ride
print("===caen: set up all channel voltages, currents and speeds for ramp===")
setRampAllChannels( boards, 30, 300, 0.04, 0.4, 5, 5 )
print("===caen: ramps are set up now: V0Set=30, V1Set=300, I0Set=0.04, I1Set=0.4, RUp=5, RDwn=5 ===")

# let's mute event mode, wait 10secs, and reset the counters
print("===pilot: mute event mode, wait===")
writePilotPropBool( "scadaCounters0", "scadaGlobalMute", 1 )
time.sleep(10)
print("===pilot: reset counters===")
writePilotPropBool( "scadaCounters0", "scadaGlobalReset", 1 ) 
print("==pilot: caen OPCUA server will now reconnect, and we wait for the s.engine to reset. 20secs===")
time.sleep(20)

# let's switch on counters
print("===pilot: enabling counters===")
writePilotPropBool( "scadaCounters0", "scadaCounterFlag", 1 ) 

# let's unmute now and switch on the counters
print("===pilot: unmute event mode, wait for caen OPCUA to reconnect===")
writePilotPropBool( "scadaCounters0", "scadaGlobalMute", 0 )
time.sleep(10)

# start the physicist ramp with Pw=on
print("===pilot: VSEL, ISEL to 0===")
writePilotPropBool( "genericPilot0", "VSEL", 0 )
writePilotPropBool( "genericPilot0", "ISEL", 0 )
print("===pilot: send Pw=on everybody, ramping up to V0===")
writePilotPropBool( "genericPilot0", "powerAllChannels", 1 )

# lets wait until V0Set is reached everywhere
print("===caen: waiting for first plateau V0Set all channels===")
waitForSetTarget( boards, "V0Set" )
print("===caen: have arrived at first plateau V0Set all channels, wait a bit===")
time.sleep(10)

# continue ramping with ISEL, VSEL
print("===pilot: VSEL, ISEL to 1, continue ramp===")
writePilotPropBool( "genericPilot0", "VSEL", 1 )
writePilotPropBool( "genericPilot0", "ISEL", 1 )
print("===caen: waiting for V1Set all channels===")
waitForSetTarget( boards, "V1Set" )
print("===caen: have arrived at second plateau V1Set all channels, wait 30secs===")
time.sleep(30)

# switch off everyone
print("===pilot: send Pw=off everybody, ramping down===")
writePilotPropBool( "genericPilot0", "powerAllChannels", 0 )

print("===caen: wait for VMon==0 on all channels===")
waitForVMonZero( boards ) 

end = time.time()
print("===all right, test is finished. It took ", end - start, " seconds===")





